<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {
	
	var $kelas = "Absensi";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_absen->getAll();
//		$data["rowData"] = [];
		$data['konten'] = "absensi/index";
		$this->load->view('template',$data);
	}

	public function lembur(){
//		$data["rowData"] = $this->M_absen->getAll();
		$data["rowData"] = [];
		$data['konten'] = "master/jabatan/lembur";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_absen->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
		
		if($id) {
            $this->M_absen->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
		else {
            $id = $this->M_absen->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

		redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_absen->getDetail($id);
        $this->M_absen->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus Jurusan ".$jurusan->nama, "Jurusan/index");
        redirect($this->kelas);
	}
}
