<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuti extends CI_Controller {
	
	var $kelas = "Cuti";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_cuti->getAll();
		$data["rowUser"] = $this->M_user->getAll();
		$data["rowIjin"] = $this->M_mst_ijin->getAll();
		$data['konten'] = "cuti/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_cuti->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["userid"] = $userid = $this->input->post("userid");
		$data["ijinid"] = $ijinid = $this->input->post("ijinid");
		$data["tanggal_pengajuan"] = $tanggal_pengjuan = $this->input->post("tanggal_pengajuan");
		$data["tanggal_mulai"] = $tanggal_mulai = $this->input->post("tanggal_mulai");
		$data["tanggal_akhir"] = $tanggal_akhir = $this->input->post("tanggal_akhir");

        $tanggal_mulai = new DateTime($tanggal_mulai);
        $tanggal_akhir = new DateTime($tanggal_akhir);
        $kurang = $tanggal_akhir->diff($tanggal_mulai)->days + 1;
		$data["total"] = $kurang;

		if($id){
            $this->M_cuti->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
		else{
            $id = $this->M_cuti->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

        redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_cuti->getDetail($id);
        $this->M_cuti->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus Jurusan ".$jurusan->nama, "Jurusan/index");
        redirect($this->kelas);
	}
}
