<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi extends CI_Controller {
	
	var $kelas = "Integrasi";

	function __construct(){
		parent::__construct();
//        if (!$this->session->userdata("id")){
//            redirect("Welcome");
//        }
//        $id = $this->session->userdata("id");
//        $this->user = $this->M_user->getDetail($id);
	}

	public function user(){
		$data = $this->input->post('data');
		$arrData = json_decode($data);
        foreach ($arrData as $row) {
            $dataAdd["noinduk"] = $noinduk = $row->noinduk;
            $dataAdd["fullname"] = $row->fullname;
            $dataAdd["status_karyawan"] = 1;
            $dataAdd["status_pajak"] = 1;
            $cek = $this->M_user->getDetailBy("noinduk = '$noinduk'");
            if(!$cek) $this->M_user->add($dataAdd);
		}

        redirect("http://mini.samaxelunicorn.com/User");
//        redirect("http://localhost/rachman/payroll_mini/User");
	}

	public function absen(){
		$data = $this->input->post('data');
        $arrData = json_decode($data);
        foreach ($arrData as $row) {
            $dataAdd["noinduk"] = $noinduk = $row->noinduk;
            $dataAdd["tanggal"] = $tanggal = $row->tanggal;
            $dataAdd["jam_masuk"] = $jam_masuk = $row->jam_masuk;
            $dataAdd["jam_keluar"] = $jam_keluar = $row->jam_keluar;
            $cek = $this->M_absen->getDetailBy("noinduk = '$noinduk' AND tanggal = '$tanggal' AND jam_masuk = '$jam_masuk' AND jam_keluar = '$jam_keluar'");
//            var_dump($cek);die;

            $jam_masuk1 = strtotime($jam_masuk);
            $jam_keluar1 = strtotime($jam_keluar);
            $kurang = $jam_keluar1-$jam_masuk1;
            $dataAdd["total_jam_kerja"] = date("H:i:s", $kurang);

            $jam_masuk2 = strtotime($jam_masuk);
            $jam_keluar2 = strtotime("09:00:00");
            $kurang = $jam_masuk2-$jam_keluar2;
            $dataAdd["terlambat"] = ($kurang > 0) ? date("H:i:s", $kurang) : "00:00:00";

            $jam_masuk3 = strtotime("18:00:00");
            $jam_keluar3 = strtotime($jam_keluar);
            $kurang = $jam_keluar3-$jam_masuk3;
            $dataAdd["lembur"] = ($kurang > 0) ? date("H:i:s", $kurang) : "00:00:00";

            if(!$cek) $this->M_absen->add($dataAdd);
        }

        redirect("http://mini.samaxelunicorn.com/Absensi");
//        redirect("http://localhost/rachman/payroll_mini/Absensi");
    }
}
