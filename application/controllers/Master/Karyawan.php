<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	var $kelas = "Master/Karyawan";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
	    $data = $this->M_user->getAllBy("roleid = 5");
//		$data["rowData"] = $this->M_user->getAllBy("roleid = 5");
		$data["rowRole"] = $this->M_role->getAll();
		$data["rowJabatan"] = $this->M_mst_jabatan->getAll();
		$data["rowData"] = $this->M_user->getAll();
		$data['konten'] = "master/karyawan/index";
		$this->load->view('template',$data);
	}

	public function lembur(){
//		$data["rowData"] = $this->M_user->getAll();
		$data["rowData"] = [];
		$data['konten'] = "master/jabatan/lembur";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_user->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("userid");
		$pass = ($this->input->post("password") != "") ? $this->input->post("password") : "asd";

		$data["username"] = $this->input->post("username");
		$data["fullname"] = $this->input->post("fullname");
		$data["jenisKelamin"] = $this->input->post("jenisKelamin");
		$data["tanggalLahir"] = $this->input->post("tanggalLahir");
		$data["roleid"] = $this->input->post("roleid");
		$data["jabatanid"] = $this->input->post("jabatanid");
		$data["npwp"] = $this->input->post("npwp");
		$data["status_karyawan"] = $this->input->post("status_karyawan");
		$data["status_pajak"] = $this->input->post("status_pajak");
		$data["gaji_pokok"] = $this->input->post("gaji_pokok");
		$data["norek"] = $this->input->post("norek");
		$data["jenisbank"] = $this->input->post("jenisbank");
		$data["password"] = $this->encrypt->encode($pass);

		if($id) {
            $this->M_user->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
		else {
            $id = $this->M_user->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

		redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_user->getDetail($id);
        $this->M_user->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus Jurusan ".$jurusan->nama, "Jurusan/index");
        redirect($this->kelas);
	}
}
