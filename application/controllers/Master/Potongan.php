<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Potongan extends CI_Controller {

    var $kelas = "Master/Potongan";

    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Login");
        }

    }

    public function index(){
        $data["rowData"] = $this->M_mst_potongan->getAll("ASC");
        $data['konten'] = "master/potongan/index";
        $this->load->view('template',$data);
    }

    public function detail($id){
        header('Content-Type: application/json');
        $rowData = $this->M_mst_potongan->getDetail($id);
        echo json_encode( $rowData );
    }

    public function add(){
        $id = $this->input->post("id");
        $data["nama"] = $this->input->post("nama");
        $data["persen"] = $this->input->post("persen");

        if($id){
            $this->M_mst_potongan->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
        else{
            $id = $this->M_mst_potongan->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

        redirect($this->kelas);
    }

    public function delete($id){
        $this->M_mst_potongan->delete($id);
        redirect($this->kelas);
    }
}
