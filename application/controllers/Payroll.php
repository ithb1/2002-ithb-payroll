<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll extends CI_Controller {
	
	var $kelas = "Payroll";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_payroll->getAll();
		$data['konten'] = "payroll/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_payroll->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$data["bulan"] = $bulan = $this->input->post("bulan");
		$data["tahun"] = $tahun = $this->input->post("tahun");
		$data["catatan"] = "-";
        $data["total_thp"] = 0;
        $data["status"] = 0;
        $this->M_payroll->add($data);
        $payrollid = $this->M_payroll->getMax("id");

        $thp = $this->kalkulasi($bulan, $tahun, $payrollid);

        $this->M_payroll->update($payrollid, array("total_thp" => $thp));
        $this->jejak->add($this->user->userid, "tambah", $this->kelas, $payrollid);

        redirect("PayrollDetail/index/".$payrollid);
	}

	public function setStatus(){
		$id = $this->input->post("id");
		$data["status"] = $this->input->post("status");
		$data["catatan"] = $this->input->post("catatan");
        $this->M_payroll->update($id,$data);
        $this->jejak->add($this->user->userid, "ubah status", $this->kelas, $id);

		redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_payroll->getDetail($id);
        $this->M_payroll->delete($id);
        $this->M_payroll_detail->deleteBy("payrollid = $id");
        redirect($this->kelas);
	}

    public function kalkulasi($bulan, $tahun, $payrollid)
    {
//        get karyawan
        $karyawan = $this->M_user->getAllBy("status_karyawan = 1");
        $persen_pph = $this->M_mst_potongan->getDetailBy("nama = 'pph'")->persen;
        $persen_jamsostek = $this->M_mst_potongan->getDetailBy("nama = 'jamsostek'")->persen;
        $persen_bpjs = $this->M_mst_potongan->getDetailBy("nama = 'bpjs'")->persen;
        $lembur1 = $this->M_mst_potongan->getDetailBy("nama = 'lembur1'")->persen;
        $lembur2 = $this->M_mst_potongan->getDetailBy("nama = 'lembur2'")->persen;

        $terlambat1a = $this->M_mst_potongan->getDetailBy("nama = 'terlambat1a'")->persen;
        $terlambat15a = $this->M_mst_potongan->getDetailBy("nama = 'terlambat15a'")->persen;
        $terlambat30a = $this->M_mst_potongan->getDetailBy("nama = 'terlambat30a'")->persen;
        $terlambat1b = $this->M_mst_potongan->getDetailBy("nama = 'terlambat1b'")->persen;
        $terlambat15b = $this->M_mst_potongan->getDetailBy("nama = 'terlambat15b'")->persen;
        $terlambat30b = $this->M_mst_potongan->getDetailBy("nama = 'terlambat30b'")->persen;

        $bulan_sebelum = str_pad($bulan-1, 2, '0', STR_PAD_LEFT);
        $bulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);

        $grupAbsen = $this->M_absen->getByQuery("SELECT DISTINCT(noinduk) FROM `absen` where tanggal LIKE '$tahun-$bulan-%'");

        $sumThp = 0;
        foreach ($grupAbsen as $gabsen){
            $item = $this->M_user->getDetailBy("noinduk = '$gabsen->noinduk'");
            $jabatan = $this->M_mst_jabatan->getDetail($item->jabatanid);
//            get absen
            $rowAbsen = $this->M_absen->getAllBy("noinduk = $item->noinduk AND (tanggal BETWEEN '$tahun-$bulan_sebelum-26' AND '$tahun-$bulan-25')");
            $sumHariKerja = 0;
            $potonganCuti = 0;
            $sumTerlambat1 = 0;
            $sumTerlambat15 = 0;
            $sumTerlambat30 = 0;
            $potonganTerlambat = 0;
            $potongan1 = 0;
            $potongan2 = 0;
            $potongan3 = 0;
            $sumLembur = 0;
            foreach ($rowAbsen as $itemAbsen) {
                $userid = $item->userid;
                $jamKerja = date("H", strtotime($itemAbsen->total_jam_kerja));

                $arr_terlambat = explode(':', $itemAbsen->terlambat);
                $terlambat = (int)$arr_terlambat[0] * 60 + (int)$arr_terlambat[1];
                $lembur = date("H", strtotime($itemAbsen->lembur));

//                jumlah hari kerja
                $sumHariKerja += ($jamKerja >= 9)?1:0;
//                terlambat
//                    1-15 menit
                if($terlambat >= 1 && $terlambat < 15) $sumTerlambat1 += 1;
//                    15 menit
                elseif($terlambat >= 15 && $terlambat < 30) $sumTerlambat15 += 1;
//                    30 menit
                elseif($terlambat >= 30) $sumTerlambat30 += 1;

//                lembur
                if($lembur == 1) $sumLembur += (($lembur * $lembur1 * $item->gaji_pokok) / 173);
                elseif($lembur > 1) $sumLembur += (($lembur * $lembur2 * $item->gaji_pokok) / 173);
            }

            if($sumHariKerja > 0){
//            cuti melahirkan
                $sumCutiMelahirkan = 0;
                $cutiMelahirkan = $this->M_cuti->getAllBy("userid = $item->userid AND tanggal_mulai LIKE '$tahun-$bulan-%'");
                foreach ($cutiMelahirkan as $itemCuti){
                    $sumCutiMelahirkan += $itemCuti->total;
                }

//            proyek
                $sumProyek = 0;
                $proyek = $this->M_proyek->getAllBy("userid = $item->userid AND tanggal_mulai LIKE '$tahun-$bulan-%'");
                foreach ($proyek as $itemProyek){
                    $sumProyek += $itemProyek->total;
                }

//            $sumHariKerja = $sumHariKerja - $sumCutiMelahirkan;

                $uang_makan = ($sumHariKerja) * $jabatan->um;
                $uang_transport = ($sumHariKerja) * $jabatan->ut;

//                operator, staff, koordinator
                if($item->jabatanid == 6 || $item->jabatanid == 7 || $item->jabatanid == 8){
                    if($sumTerlambat1) $potongan1 = $sumTerlambat1 * ($terlambat1a * $item->gaji_pokok);
                    elseif($sumTerlambat15) $potongan2 = $sumTerlambat15 * ($terlambat15a * $item->gaji_pokok);
                    elseif($sumTerlambat30) $potongan3 = $terlambat30a * ($sumTerlambat30/$sumHariKerja) * ($item->gaji_pokok+$jabatan->tunjangan);
                }
//                spv, sectionchief, manager, gm, director
                else{
                    if($sumTerlambat1) $potongan1 = $sumTerlambat1 * ($terlambat1b * $jabatan->ut);
                    elseif($sumTerlambat15) $potongan2 = $sumTerlambat15 * ($terlambat15b * $jabatan->ut);
                    elseif($sumTerlambat30) $potongan3 = $terlambat30b * ($sumTerlambat30/$sumHariKerja) * ($item->gaji_pokok+$jabatan->tunjangan);
                }

                $potonganTerlambat = $potongan1+$potongan2+$potongan3;

//            add to detail
                $dataPayDetAdd['payrollid'] = $payrollid;
                $dataPayDetAdd['userid'] = $item->userid;
//            penambah
                $gaji_pokok = round($item->gaji_pokok,2);
                $tunjangan_jabatan = round($jabatan->tunjangan,2);
                $tunjangan_dinas = $jabatan->tunjangan_proyek * $sumProyek;
                $lembur = $sumLembur;
                $tambahan_lain = 0;
//            pengurang

                $jamsostek = ($persen_jamsostek) * $gaji_pokok;
                $pph = ($gaji_pokok > 4500000)? ($persen_pph) * $gaji_pokok : 0;
                $bpjs = ($persen_bpjs) * $gaji_pokok;
                $pinjaman = 0;
                $potongan_lain = $potonganTerlambat + $potonganCuti; //telat dan cuti

                $total_penambah = $gaji_pokok + $tunjangan_jabatan + $uang_makan + $uang_transport + $tunjangan_dinas + $lembur + $tambahan_lain;
                $total_pengurang = $jamsostek + $pph + $bpjs + $pinjaman + $potongan_lain;

                $takehomepay = $total_penambah - $total_pengurang;

                $lembur = round($lembur,2);
                $jamsostek = round($jamsostek,2);
                $pph = round($pph,2);
                $bpjs = round($bpjs,2);
                $total_penambah = round($total_penambah,2);
                $total_pengurang = round($total_pengurang,2);
                $takehomepay = round($takehomepay,2);
            }else{
                $gaji_pokok = 0;
                $tunjangan_jabatan = 0;
                $uang_makan = 0;
                $uang_transport = 0;
                $tunjangan_dinas = 0;
                $lembur = 0;
                $tambahan_lain = 0;
                $jamsostek = 0;
                $pph = 0;
                $bpjs = 0;
                $pinjaman = 0;
                $potonganTerlambat = 0;
                $potongan_lain = 0;
                $total_penambah = 0;
                $total_pengurang = 0;
                $takehomepay = 0;
            }
//            =====================================
//            add to detail
            $dataPayDetAdd['payrollid'] = $payrollid;
            $dataPayDetAdd['userid'] = $userid;
//            penambah
            $dataPayDetAdd['gaji_pokok'] = $gaji_pokok;
            $dataPayDetAdd['tunjangan_jabatan'] = $tunjangan_jabatan;
            $dataPayDetAdd['uang_makan'] = $uang_makan;
            $dataPayDetAdd['uang_transport'] = $uang_transport;
            $dataPayDetAdd['tunjangan_dinas'] = $tunjangan_dinas;
            $dataPayDetAdd['lembur'] = $lembur;
            $dataPayDetAdd['tambahan_lain'] = $tambahan_lain;

//            pengurang
            $dataPayDetAdd['jamsostek'] = $jamsostek;
            $dataPayDetAdd['pph'] = $pph;
            $dataPayDetAdd['bpjs'] = $bpjs;
            $dataPayDetAdd['pinjaman'] = $pinjaman;
            $dataPayDetAdd['potonganTerlambat'] = $potonganTerlambat;
            $dataPayDetAdd['potongan_lain'] = $potongan_lain;

            $dataPayDetAdd['total_penambah'] = $total_penambah;
            $dataPayDetAdd['total_pengurang'] = $total_pengurang;

            $dataPayDetAdd['takehomepay'] = $takehomepay;
            $dataPayDetAdd['total_hari_kerja'] = $sumHariKerja;
            $dataPayDetAdd['keterangan'] = "-";

            $this->M_payroll_detail->add($dataPayDetAdd);

            $sumThp += $takehomepay;
        }
        return $sumThp;
	}
}
