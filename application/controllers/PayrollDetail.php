<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PayrollDetail extends CI_Controller {
	
	var $kelas = "PayrollDetail";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index($payrollid){
        $data["dataPayroll"] = $this->M_payroll->getDetail($payrollid);
        $data["rowData"] = $this->M_payroll_detail->getAllBy("payrollid = $payrollid");
        $data['konten'] = "payroll/detail";
        $this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_payroll_detail->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function edit(){
        $id = $this->input->post("id");
        $payrollid = $this->input->post("payrollid");
		$data["gaji_pokok"] = $gaji_pokok = $this->input->post("gaji_pokok");
		$data["jamsostek"] = $jamsostek = $this->input->post("jamsostek");
		$data["tunjangan_jabatan"] = $tunjangan_jabatan = $this->input->post("tunjangan_jabatan");
		$data["pph"] = $pph = $this->input->post("pph");
		$data["uang_makan"] = $uang_makan = $this->input->post("uang_makan");
		$data["bpjs"] = $bpjs = $this->input->post("bpjs");
		$data["uang_transport"] = $uang_transport = $this->input->post("uang_transport");
		$data["pinjaman"] = $pinjaman = $this->input->post("pinjaman");
		$data["tunjangan_dinas"] = $tunjangan_dinas = $this->input->post("tunjangan_dinas");
		$data["lembur"] = $lembur = $this->input->post("lembur");
		$data["tambahan_lain"] = $tambahan_lain = $this->input->post("tambahan_lain");
		$data["potongan_lain"] = $potongan_lain = $this->input->post("potongan_lain");

        $total_penambah = $gaji_pokok + $tunjangan_jabatan + $uang_makan + $uang_transport + $tunjangan_dinas + $lembur + $tambahan_lain;
        $total_pengurang = $jamsostek + $pph + $bpjs + $pinjaman + $potongan_lain;
        $takehomepay = $total_penambah - $total_pengurang;

		$data["total_penambah"] = $total_penambah = $this->input->post("total_penambah");
		$data["total_pengurang"] = $total_pengurang = $this->input->post("total_pengurang");
		$data["takehomepay"] = $takehomepay;

		$data["keterangan"] = $keterangan = $this->input->post("keterangan");
        $this->M_payroll_detail->update($id,$data);
        $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);

        redirect($this->kelas."/index/".$payrollid);
	}

	public function print(){
        $id = $this->input->post("id");
        $data["payDetail"] = $payrollDetail = $this->M_payroll_detail->getDetail($id);
        $data["pay"] = $pay = $this->M_payroll->getDetail($payrollDetail->payrollid);
        $data["user"] = $user = $this->M_user->getDetail($payrollDetail->userid);
        $data['konten'] = "page/payroll/print";
        $data['filename'] = "Slip Gaji ".$user->noinduk."-".$user->fullname;
        $this->load->view("page/export/templatePdf",$data);
	}

	public function setPajak(){
		$payrollid = $this->input->post("payrollid");
		$id = $this->input->post("id");
		$pph_awal  = $this->input->post("pph_awal");
		$pph_ubah  = $this->input->post("pph_ubah");
		$total_pengurang  = $this->input->post("total_pengurang");
		$takehomepay  = $this->input->post("takehomepay");
		$takehomepay_ubah = ($takehomepay + $pph_awal) - $pph_ubah;

		$data["pph"] = $pph_ubah;
		$data["total_pengurang"] = ($total_pengurang - $pph_awal) + $pph_ubah;
		$data["takehomepay"] = $takehomepay_ubah;
        $this->M_payroll_detail->update($id,$data);

        $payroll = $this->M_payroll->getDetail($payrollid);
        $total_thp = ($payroll->total_thp - $takehomepay)+$takehomepay_ubah;
        $this->M_payroll->update($payrollid,array("total_thp" => $total_thp));

		redirect($this->kelas."/index/".$payrollid);
	}

	public function delete($id){
        $jurusan = $this->M_payroll->getDetail($id);
        $this->M_payroll->delete($id);
        redirect($this->kelas);
	}
}
