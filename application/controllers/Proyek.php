<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyek extends CI_Controller {
	
	var $kelas = "Proyek";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
		$data["rowData"] = $this->M_proyek->getAll();
		$data["rowUser"] = $this->M_user->getAll();
		$data['konten'] = "proyek/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_proyek->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["userid"] = $userid = $this->input->post("userid");
		$data["tanggal_pengajuan"] = $tanggal_pengjuan = $this->input->post("tanggal_pengajuan");
		$data["tanggal_mulai"] = $tanggal_mulai = $this->input->post("tanggal_mulai");
		$data["tanggal_akhir"] = $tanggal_akhir = $this->input->post("tanggal_akhir");

        $tanggal_mulai = new DateTime($tanggal_mulai);
        $tanggal_akhir = new DateTime($tanggal_akhir);
        $kurang = $tanggal_akhir->diff($tanggal_mulai)->days + 1;
		$data["total"] = $kurang;

		if($id){
            $this->M_proyek->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
		else{
            $id = $this->M_proyek->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

		redirect($this->kelas);
	}

	public function delete($id){
        $this->M_proyek->delete($id);
        redirect($this->kelas);
	}
}
