<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

    var $kelas = "Master/Log";

    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Login");
        }

    }

    public function index(){
        $data["rowData"] = $this->M_log->getAll("ASC");
        $data['konten'] = "master/log/index";
        $this->load->view('template',$data);
    }

    public function detail($id){
        header('Content-Type: application/json');
        $rowData = $this->M_log->getDetail($id);
        echo json_encode( $rowData );
    }

    public function add(){
        $id = $this->input->post("id");
        $data["nama"] = $this->input->post("nama");
        $data["tunjangan_proyek"] = $this->input->post("tunjangan_proyek");
        $data["tunjangan"] = $this->input->post("tunjangan");
        $data["ut"] = $this->input->post("ut");
        $data["um"] = $this->input->post("um");

        if($id){
            $this->M_log->update($id,$data);
            $this->jejak->add($this->user->userid, "ubah", $this->kelas, $id);
        }
        else{
            $id = $this->M_log->add($data);
            $this->jejak->add($this->user->userid, "tambah", $this->kelas, $id);
        }

        redirect($this->kelas);
    }

    public function delete($id){
        $this->M_log->delete($id);
        redirect($this->kelas);
    }
}
