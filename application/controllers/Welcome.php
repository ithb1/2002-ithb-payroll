<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata("id")) {
            redirect("Login");
        }

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
    }

    public function index()
    {
//        $this->session->set_userdata("id",1);
//        $this->session->set_userdata("error",false);
//        $this->session->set_userdata("welcome",TRUE);
        $data["konten"] = "dashboard";
        $this->load->view('template', $data);
    }

    public function profil()
    {
        if ($this->input->post("btnsubmit")) {
            $data["nama"] = $this->input->post("nama");
            if ($this->input->post("password")) {
                $data["password"] = $this->encrypt->encode($this->input->post("password"));
            }
            $this->M_user->update($this->user->userid, $data);
            redirect("Welcome/profil");
        }

        $data['user'] = $this->user;
        $data['konten'] = "profil";
        $this->load->view('template', $data);
    }

    public function export()
    {
        $data['filename'] = "Laporan";
        $data["rowData"] = $this->M_user->getAll();
        $data['konten'] = "page/export/user";
        $this->load->view("page/export/templatePdf", $data);
    }

    public function exportExcel($id)
    {
        $payroll = $this->M_payroll->getDetail($id);
        $payrollDetail = $this->M_payroll_detail->getAllBy("payrollid = $id");

        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Panggil class PHPExcel nya
        $excel = new PHPExcel();
        // Settingan awal fil excel
        $excel->getProperties()->setCreator('http://www.samaxelunicorn.com/')
            ->setLastModifiedBy('http://www.samaxelunicorn.com/')
            ->setTitle("Payroll")
            ->setSubject("Karyawan")
            ->setDescription("Laporan Payroll")
            ->setKeywords("Data Payroll");

        $style_col = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA PAYROLL");
        $excel->getActiveSheet()->mergeCells('A1:E1');
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('A2', "Periode : ".$payroll->bulan."-".$payroll->tahun);
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "Total Takehomepay : ".$payroll->total_thp);

        $excel->setActiveSheetIndex(0)->setCellValue('A5', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B5', "NIK");
        $excel->setActiveSheetIndex(0)->setCellValue('C5', "NAMA");
        $excel->setActiveSheetIndex(0)->setCellValue('D5', "NOREK");
        $excel->setActiveSheetIndex(0)->setCellValue('E5', "TOTAL TAKEHOMEPAY");
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A5')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B5')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C5')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D5')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E5')->applyFromArray($style_col);

        $no = 1;
        $numrow = 6;
        foreach ($payrollDetail as $data) {
            $user = $this->M_user->getDetail($data->userid);
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $user->noinduk);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $user->fullname);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $user->norek);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, "Rp ".number_format($data->takehomepay,'0', ",",'.'));

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);

            $no++;
            $numrow++;
        }
        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom D

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $excel->getActiveSheet(0)->setTitle("Laporan Data Payroll");
        $excel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Payroll.xlsx"');
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');

    }
}
