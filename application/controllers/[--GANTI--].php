<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GANTI extends CI_Controller {
	
	var $kelas = "GANTI";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Backend/Login");
		}

		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);
	}

	public function index(){

		$data["rowData"] = $this->M_user->getAll();
		$data['konten'] = "user/index";
		$this->load->view('template',$data);
	}

	public function detail($id){

		$data["data"] = $this->M_user->getDetail($id);
		$data['konten'] = "user/detail";
		$this->load->view('template',$data);
	}

	public function add(){
		if($this->input->post("btnsubmit")){
			$data["fullname"] = $this->input->post("fullname");
			$this->M_user->add($data);
			redirect($this->kelas);
		}
		$data['konten'] = "user/index_add";
		$this->load->view('template',$data);
	}

	public function update($id){
		if($this->input->post("btnsubmit")){
			$data["fullname"] = $this->input->post("fullname");
			$this->M_user->update($id,$data);
			redirect($this->kelas);
		}

		$data["data"] = $this->M_user->getDetail($id);
		$data['konten'] = "user/index";
		$this->load->view('template',$data);
	}

	public function delete($id){		
		$this->M_user->delete($id);
		redirect($this->kelas);
	}
}
