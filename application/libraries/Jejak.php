<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jejak {

    var $table_name = "sys_log";

    public function __get($var) {
        return get_instance()->$var;
    }

    public function add($userid, $aksi, $modul, $modulid = 0) {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            "userid" => $userid,
            "aksi" => $aksi,
            "modul" => $modul,
            "modulid" => $modulid,
            "waktu" => date("Y-m-d H-i-s"),
            );
        $this->db->insert($this->table_name, $data);
    }

}
