
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Absensi
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Absensi </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Tanggal</th>
              <th>Jam Masuk</th>
              <th>Jam Keluar</th>
              <th>Total Jam Kerja</th>
              <th>Keterlambatan</th>
              <th>Lembur</th>
            </tr>
            </thead>
            <tbody><?php
            $no = 1;
            foreach ($rowData as $row) :
                $user = $this->M_user->getDetailBy("noinduk = '$row->noinduk'");
            ?>
            <tr>
                <td><?=$no++?></td>
                <td> <?=$row->noinduk?> </td>
                <td> <?=$user->fullname?> </td>
                <td> <?=($user->jabatanid) ? $this->M_mst_jabatan->getDetail($user->jabatanid)->nama : ""?> </td>
                <td align="center"> <?=$row->tanggal?> </td>
                <td align="center"> <?=$row->jam_masuk?> </td>
                <td align="center"> <?=$row->jam_keluar?> </td>
                <td align="center"> <?=date("H", strtotime($row->total_jam_kerja))?> jam, <?=date("i", strtotime($row->total_jam_kerja))?> menit </td>
                <td align="center"> <?=date("H", strtotime($row->terlambat))?> jam, <?=date("i", strtotime($row->terlambat))?> menit </td>
                <td align="center"> <?=date("H", strtotime($row->lembur))?> jam, <?=date("i", strtotime($row->lembur))?> menit </td>
            </tr>
            <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Kategori</h4>
      </div>
      <?=form_open("Kategori/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="kategoriid" placeholder="kategoriid" name="kategoriid" value="">
                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                  </div>
                </div>           
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Kategori/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#kategoriid').val(id).hide();
         $('#nama').val(data.nama);
        }
    });
  }

  function clearForm() {    
     $('#kategoriid').val("");
     $('#nama').val("");
  }
</script>