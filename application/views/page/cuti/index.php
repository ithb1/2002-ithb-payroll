
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Cuti
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Cuti </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Jenis Ijin</th>
              <th>Tanggal Pengajuan</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Akhir</th>
              <th>Total Waktu</th>
              <th>action</th>
            </tr>
            </thead>
              <tbody>
              <?php
              $no = 1;
              foreach ($rowData as $row) :
                  $user = $this->M_user->getDetail($row->userid);
                  ?>
                  <tr>
                      <td><?=$no++;?></td>
                      <td><?=$user->noinduk;?></td>
                      <td><?=$user->fullname;?></td>
                      <td><?=$this->M_mst_ijin->getDetail($row->ijinid)->nama;?></td>
                      <td><?=$row->tanggal_pengajuan;?></td>
                      <td><?=$row->tanggal_mulai;?></td>
                      <td><?=$row->tanggal_akhir;?></td>
                      <td><?=$row->total." hari";?></td>
                      <td>
                          <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                          <a href="<?=site_url('Cuti/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                  </tr>
              <?php endforeach;?>
              </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Cuti</h4>
      </div>
      <?=form_open("Cuti/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="userid" class="col-sm-4 control-label">Karyawan</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                      <select name="userid" id="userid" required class="form-control">
                          <option value="">- user -</option>
                          <?php foreach($rowUser as $row):?>
                              <option value="<?=$row->userid?>" ><?=$row->noinduk." - ".$row->fullname?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ijinid" class="col-sm-4 control-label">Ijin</label>
                  <div class="col-sm-8">
                      <select name="ijinid" id="ijinid" required class="form-control">
                          <option value="">- ijin -</option>
                          <?php foreach($rowIjin as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggal_pengajuan" class="col-sm-4 control-label">Tanggal Pengajuan</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggal_pengajuan" placeholder="tanggal_pengajuan" name="tanggal_pengajuan" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggal_mulai" class="col-sm-4 control-label">Tanggal Mulai</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggal_mulai" placeholder="tanggal_mulai" name="tanggal_mulai" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggal_akhir" class="col-sm-4 control-label">Tanggal Akhir</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggal_akhir" placeholder="tanggal_akhir" name="tanggal_akhir" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
      var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Cuti/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#id').val(id).hide();
         $('#userid').val(data.userid);
         $('#ijinid').val(data.ijinid);
         $('#tanggal_pengajuan').val(data.tanggal_pengajuan);
         $('#tanggal_mulai').val(data.tanggal_mulai);
         $('#tanggal_akhir').val(data.tanggal_akhir);
      }
    });
  }

  function clearForm() {    
        $('#id').val("");
        $('#userid').val("");
        $('#ijinid').val("");
        $('#tanggal_pengajuan').val("");
        $('#tanggal_mulai').val("");
        $('#tanggal_akhir').val("");
  }
</script>