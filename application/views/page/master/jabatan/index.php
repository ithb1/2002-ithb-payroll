
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Jabatan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=""> Jabatan </a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Tabel Data
                    </h3>
                    <div class="pull-right">
                        <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tunjangan</th>
                            <th>Tunjangan Proyek</th>
                            <th>Uang Transport</th>
                            <th>Uang Makan</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?=$row->nama;?></td>
                                <td><?=number_format($row->tunjangan,0);?></td>
                                <td><?=number_format($row->tunjangan_proyek,0);?></td>
                                <td><?=number_format($row->ut,0);?></td>
                                <td><?=number_format($row->um,0);?></td>
                                <td>
                                    <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                                    <a href="<?=site_url('Master/Jabatan/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Jabatan</h4>
            </div>
            <?=form_open("Master/Jabatan/add","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tunjangan" class="col-sm-4 control-label">Tunjangan</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="tunjangan" placeholder="tunjangan" name="tunjangan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tunjangan_proyek" class="col-sm-4 control-label">Tunjangan Proyek</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="tunjangan_proyek" placeholder="tunjangan_proyek" name="tunjangan_proyek" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ut" class="col-sm-4 control-label">Uang Transport</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="ut" placeholder="ut" name="ut" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="um" class="col-sm-4 control-label">Uang Makan</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="um" placeholder="um" name="um" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Master/Jabatan/detail/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#nama').val(data.nama);
                $('#tunjangan_proyek').val(data.tunjangan_proyek);
                $('#tunjangan').val(data.tunjangan);
                $('#ut').val(data.ut);
                $('#um').val(data.um);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#nama').val("");
        $('#tunjangan_proyek').val("");
        $('#tunjangan').val("");
        $('#ut').val("");
        $('#um').val("");
    }
</script>