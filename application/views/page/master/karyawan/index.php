<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Karyawan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Karyawan </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
<!--            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Username</th>
              <th>Nama</th>
              <th>No Rekening</th>
              <th>NPWP</th>
              <th>Status Karyawan</th>
              <th>Status Pajak</th>
              <th>Jabatan</th>
              <th>Gaji Pokok</th>
              <th>ROLE</th>
              <th>action</th>
            </tr>
            </thead>
              <tbody>
              <?php
              $no = 1;
              foreach ($rowData as $row) :
                  ?>
                  <tr>
                      <td><?=$no++;?></td>
                      <td><?=$row->noinduk;?></td>
                      <td><?=$row->username;?></td>
                      <td><?=$row->fullname;?></td>
                      <td><?="(".$row->jenisbank.") ".$row->norek;?></td>
                      <td><?=$row->npwp;?></td>
                      <td align="center">
                          <?php if($row->status_karyawan):?>
                            <span class="label label-success">AKTIF</span>
                          <?php else: ?>
                              <span class="label label-danger">NON AKTIF</span>
                          <?php endif; ?>
                      </td>
                      <td align="center">
                          <?php if($row->status_pajak):?>
                              <span class="label label-success">KENA PAJAK</span>
                          <?php else: ?>
                              <span class="label label-danger">TIDAK KENA PAJAK</span>
                          <?php endif; ?>
                      </td>
                      <td><?=($row->jabatanid) ? $this->M_mst_jabatan->getDetail($row->jabatanid)->nama : "";?></td>
                      <td><?=number_format($row->gaji_pokok,0);?></td>
                      <td><?=($row->roleid) ? $this->M_role->getDetail($row->roleid)->nama : "";?></td>
                      <td>
                          <a href="" data-id="<?=$row->userid?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                      </td>
                  </tr>
              <?php endforeach;?>
              </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Karyawan</h4>
      </div>
      <?=form_open("Master/Karyawan/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="fullname" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="userid" placeholder="userid" name="userid" value="">
                    <input type="text" class="form-control" id="fullname" placeholder="fullname" name="fullname" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="jenisKelamin" class="col-sm-4 control-label">JENIS KELAMIN</label>
                  <div class="col-sm-8">
                      <select name="jenisKelamin" id="jenisKelamin" required class="form-control">
                          <option value="">- jenis kelamin -</option>
                          <option value="l">Laki-laki</option>
                          <option value="p">Perempuan</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggalLahir" class="col-sm-4 control-label">TANGGAL LAHIR</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggalLahir" placeholder="tanggalLahir" name="tanggalLahir" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="roleid" class="col-sm-4 control-label">ROLE</label>
                  <div class="col-sm-8">
                      <select name="roleid" id="roleid" required class="form-control">
                          <option value="">- role -</option>
                          <?php foreach($rowRole as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="jabatanid" class="col-sm-4 control-label">JABATAN</label>
                  <div class="col-sm-8">
                      <select name="jabatanid" id="jabatanid" required class="form-control">
                          <option value="">- jabatan -</option>
                          <?php foreach($rowJabatan as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="norek" class="col-sm-4 control-label">Nomor Rekening</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="norek" placeholder="norek" name="norek" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="jenisbank" class="col-sm-4 control-label">Jenis Bank</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="jenisbank" placeholder="jenisbank" name="jenisbank" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="npwp" class="col-sm-4 control-label">NPWP</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="npwp" placeholder="npwp" name="npwp" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="status_karyawan" class="col-sm-4 control-label">STATUS KARYAWAN</label>
                  <div class="col-sm-8">
                      <select name="status_karyawan" id="status_karyawan" required class="form-control">
                          <option value="">- status karyawan -</option>
                          <option value="1">AKTIF</option>
                          <option value="0">NON AKTIF</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="status_pajak" class="col-sm-4 control-label">STATUS PAJAK</label>
                  <div class="col-sm-8">
                      <select name="status_pajak" id="status_pajak" required class="form-control">
                          <option value="">- status pajak -</option>
                          <option value="1">KENA PAJAK</option>
                          <option value="0">TIDAK KENA PAJAK</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="gaji_pokok" class="col-sm-4 control-label">GAJI POKOK</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="gaji_pokok" placeholder="gaji_pokok" name="gaji_pokok" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-4 control-label">USERNAME</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="username" placeholder="username" name="username" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-4 control-label">PASSWORD</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" placeholder="password" name="password" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Master/Karyawan/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#userid').val(id).hide();
         $('#fullname').val(data.fullname);
         $('#jenisKelamin').val(data.jenisKelamin);
         $('#tanggalLahir').val(data.tanggalLahir);
         $('#roleid').val(data.roleid);
         $('#jabatanid').val(data.jabatanid);
         $('#npwp').val(data.npwp);
         $('#status_karyawan').val(data.status_karyawan);
         $('#status_pajak').val(data.status_pajak);
         $('#gaji_pokok').val(data.gaji_pokok);
         $('#username').val(data.username);
         $('#norek').val(data.norek);
         $('#jenisbank').val(data.jenisbank);
        }
    });
  }

  function clearForm() {    
     $('#userid').val("");
     $('#fullname').val("");
     $('#jenisKelamin').val("");
     $('#tanggalLahir').val("");
     $('#roleid').val("");
     $('#jabatanid').val("");
     $('#npwp').val("");
     $('#status_karyawan').val("");
     $('#status_pajak').val("");
     $('#gaji_pokok').val("");
     $('#username').val("");
     $('#password').val("");
     $('#norek').val("");
     $('#jenisbank').val("");
  }
</script>