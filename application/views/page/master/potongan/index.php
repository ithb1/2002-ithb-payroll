
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Potongan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=""> Potongan </a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Tabel Data
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Nilai</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?=$row->nama;?></td>
                                <td><?=$row->persen;?></td>
                                <td>
                                    <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Potongan</h4>
            </div>
            <?=form_open("Master/Potongan/add","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="persen" class="col-sm-4 control-label">Nilai</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="persen" placeholder="persen" name="persen" value="" step="0.01">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Master/Potongan/detail/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#nama').val(data.nama);
                $('#persen').val(data.persen);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#nama').val("");
        $('#persen').val("");
    }
</script>