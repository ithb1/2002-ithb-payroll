<?php
$iduser = $this->session->userdata("id");
$userLogin = $this->M_user->getDetail($iduser);

$periode = "";
switch ($dataPayroll->bulan){
    case 1: $periode = "Januari "; break;
    case 2: $periode = "Februari "; break;
    case 3: $periode = "Maret "; break;
    case 4: $periode = "April "; break;
    case 5: $periode = "Mei "; break;
    case 6: $periode = "Juni "; break;
    case 7: $periode = "Juli "; break;
    case 8: $periode = "Agustus "; break;
    case 9: $periode = "September "; break;
    case 10: $periode = "Oktober "; break;
    case 11: $periode = "November "; break;
    case 12: $periode = "Desember "; break;
}

$periode .= $dataPayroll->tahun;
?>

<style>
    tr, td {
        padding: 5px;
    }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Detail Payroll
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Payroll </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
              TOTAL TAKE HOME PAY : <?="Rp ".number_format($dataPayroll->total_thp,0);?>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Gaji Pokok</th>
              <th>Penambah</th>
              <th>Pengurang</th>
              <th>Pajak</th>
              <th>Take Home Pay</th>
              <th>action</th>
            </tr>
            </thead>
              <tbody>
              <?php
              $no = 1;
              foreach ($rowData as $row) :
                  $user = $this->M_user->getDetail($row->userid);
                  ?>
                  <tr>
                      <td><?=$no++;?></td>
                      <td><?=$user->noinduk;?></td>
                      <td><?=$user->fullname;?></td>
                      <td><?=$this->M_mst_jabatan->getDetail($user->jabatanid)->nama;?></td>
                      <td><?="Rp ".number_format($row->gaji_pokok, 0);?></td>
                      <td><?="Rp ".number_format($row->total_penambah, 0);?></td>
                      <td><?="Rp ".number_format($row->total_pengurang, 0);?></td>
                      <td><?="Rp ".number_format($row->pph, 0);?></td>
                      <td><?="Rp ".number_format($row->takehomepay, 0);?></td>
                      <td>
                          <?php if($userLogin->roleid == 1 || $userLogin->roleid == 2 || $userLogin->roleid == 3): ?>
                          <a href="" data-id="<?=$row->id?>" data-userid="<?=$user->userid?>" data-toggle="modal" data-target="#formPrint" onclick="getDetail(this)" class="btn btn-xs btn-info"><i class="fa fa-info"></i> detail </a>
                          <a href="" data-id="<?=$row->id?>" data-userid="<?=$user->userid?>" data-toggle="modal" data-target="#formEdit" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                          <?php else: ?>
                          <a href="" data-id="<?=$row->id?>" data-userid="<?=$user->userid?>" data-toggle="modal" data-target="#formPajak" onclick="getDetail(this)" class="btn btn-xs btn-danger"><i class="fa fa-dollar"></i></a>
                          <?php endif; ?>
                      </td>
                  </tr>
              <?php endforeach;?>
              </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="formPrint" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Slip Gaji</h4>
          </div>
          <?=form_open("PayrollDetail/print");?>
        <input type="hidden" class="id" name="id">
          <div class="modal-body">

              <div class="box-body">
                  <div class="row">
                      <div class="col-md-12">
                          <table border="0" width="100%">
                              <tr>
                                  <td width="15%">Periode</td>
                                  <td width="30%">: <input type="text" class="periode" readonly></td>
                                  <td width="15%">NIK</td>
                                  <td width="30%">: <input type="text" class="noinduk" readonly></td>
                              </tr>
                              <tr>
                                  <td>Kehadiran</td>
                                  <td>: <input type="text" class="total_hari_kerja" readonly></td>
                                  <td>Nama</td>
                                  <td>: <input type="text" class="fullname" readonly></td>
                              </tr>
                              <tr>
                                  <td>Status Pajak</td>
                                  <td>: <input type="text" class="status_pajak" readonly></td>
                                  <td>NPWP</td>
                                  <td>: <input type="text" class="npwp" readonly></td>
                              </tr>
                          </table>
                      </div>
                      &nbsp
                      <div class="col-md-12">
                          <table width="100%" border="1" cellpadding="10" cellspacing="10">
                              <tr>
                                  <td align="center" colspan="2"><b>PENAMBAH</b> </td>
                                  <td align="center"><b>PENGURANG</b> </td>
                              </tr>
                              <tr>
                                  <td rowspan="2">Tetap</td>
                                  <td>Gaji Pokok : <br> <input type="text" class="gaji_pokok" readonly> </td>
                                  <td>Jamsostek (2%) : <br> <input type="text" class="jamsostek" readonly> </td>
                              </tr>
                              <tr>
                                  <td>Tunjangan Jabatan : <br> <input type="text" class="tunjangan_jabatan" readonly> </td>
                                  <td>Pph : <br> <input type="text" class="pph" readonly> </td>
                              </tr>
                              <tr>
                                  <td rowspan="3">Variabel</td>
                                  <td>Uang Makan : <br> <input type="text" class="uang_makan" readonly> </td>
                                  <td>BPJS Kesehatan (1%) : <br> <input type="text" class="bpjs" readonly> </td>
                              </tr>
                              <tr>
                                  <td>Uang Transport : <br> <input type="text" class="uang_transport" readonly> </td>
                                  <td>Pinjaman : <br> <input type="text" class="pinjaman" readonly> </td>
                              </tr>
                              <tr>
                                  <td>Tunjangan Proyek/Dinas : <br> <input type="text" class="tunjangan_dinas" readonly> </td>
                                  <td rowspan="2"></td>
                              </tr>
                              <tr>
                                  <td colspan="2">Lembur : <br> <input type="text" class="lembur" readonly> </td>
                              </tr>
                              <tr>
                                  <td colspan="2">Tambahan Lain : <br> <input type="text" class="tambahan_lain" readonly> </td>
                                  <td>Potongan Lain : <br> <input type="text" class="potongan_lain" readonly> </td>
                              </tr>
                              <tr>
                                  <td colspan="2"><b>TOTAL : <br> <input type="text" class="total_penambah" readonly> </b></td>
                                  <td><b>TOTAL : <br> <input type="text" class="total_pengurang" readonly> </b></td>
                              </tr>
                              <tr>
                                  <td colspan="3">Take Home Pay : <br> <input type="text" class="takehomepay" readonly> </td>
                              </tr>
                              <tr>
                                  <td colspan="3">Keterangan : <br> <input type="text" class="keterangan" readonly> </td>
                              </tr>
                          </table>
                      </div>
                  </div>

              </div>
              <!-- /.box-footer -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success" formtarget="_blank"><i class="fa fa-print"></i> print</button>
          </div>
        <?=form_close();?>
    </div>

  </div>
</div>




<!-- Modal -->
<div id="formEdit" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Slip Gaji</h4>
            </div>
            <?=form_open("PayrollDetail/edit");?>
            <input type="hidden" class="id" name="id">
            <input type="hidden" class="payrollid" name="payrollid">
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table border="0" width="100%">
                                <tr>
                                    <td width="15%">Periode</td>
                                    <td width="30%">: <input type="text" class="periode" readonly></td>
                                    <td width="15%">NIK</td>
                                    <td width="30%">: <input type="text" class="noinduk" readonly></td>
                                </tr>
                                <tr>
                                    <td>Kehadiran</td>
                                    <td>: <input type="text" class="total_hari_kerja" readonly></td>
                                    <td>Nama</td>
                                    <td>: <input type="text" class="fullname" readonly></td>
                                </tr>
                                <tr>
                                    <td>Status Pajak</td>
                                    <td>: <input type="text" class="status_pajak" readonly></td>
                                    <td>NPWP</td>
                                    <td>: <input type="text" class="npwp" readonly></td>
                                </tr>
                            </table>
                        </div>
                        &nbsp
                        <div class="col-md-12">
                            <table width="100%" border="1" cellpadding="10" cellspacing="10">
                                <tr>
                                    <td align="center" colspan="2"><b>PENAMBAH</b> </td>
                                    <td align="center"><b>PENGURANG</b> </td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Tetap</td>
                                    <td>Gaji Pokok : <br> <input type="number" step="0.01" class="gaji_pokok" name="gaji_pokok"> </td>
                                    <td>Jamsostek (2%) : <br> <input type="number" step="0.01" class="jamsostek" name="jamsostek"> </td>
                                </tr>
                                <tr>
                                    <td>Tunjangan Jabatan : <br> <input type="number" step="0.01" class="tunjangan_jabatan" name="tunjangan_jabatan"> </td>
                                    <td>Pph : <br> <input type="number" step="0.01" class="pph" name="pph"> </td>
                                </tr>
                                <tr>
                                    <td rowspan="3">Variabel</td>
                                    <td>Uang Makan : <br> <input type="number" step="0.01" class="uang_makan" name="uang_makan"> </td>
                                    <td>BPJS Kesehatan (1%) : <br> <input type="number" step="0.01" class="bpjs" name="bpjs"> </td>
                                </tr>
                                <tr>
                                    <td>Uang Transport : <br> <input type="number" step="0.01" class="uang_transport" name="uang_transport"> </td>
                                    <td>Pinjaman : <br> <input type="number" step="0.01" class="pinjaman" name="pinjaman"> </td>
                                </tr>
                                <tr>
                                    <td>Tunjangan Proyek/Dinas : <br> <input type="number" step="0.01" class="tunjangan_dinas" name="tunjangan_dinas"> </td>
                                    <td>Potongan Terlambat : <br> <input type="number" step="0.01" class="potonganTerlambat" name="potonganTerlambat"> </td>
                                </tr>
                                <tr>
                                    <td colspan="2">Lembur : <br> <input type="number" step="0.01" class="lembur" name="lembur"> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Tambahan Lain : <br> <input type="number" step="0.01" class="tambahan_lain" name="tambahan_lain"> </td>
                                    <td>Potongan Lain : <br> <input type="number" step="0.01" class="potongan_lain" name="potongan_lain"> </td>
                                </tr>
                                <tr>
                                    <td colspan="3">Keterangan : <br> <input type="text" class="keterangan" name="keterangan"> </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>
<div id="formPajak" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Payroll</h4>
            </div>
            <?=form_open("PayrollDetail/setPajak","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="bulan" class="col-sm-4 control-label">Pajak</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="hidden" class="form-control payrollid" placeholder="payrollid" name="payrollid" value="">
                                    <input type="hidden" class="form-control total_pengurang" placeholder="total_pengurang" name="total_pengurang" value="">
                                    <input type="hidden" class="form-control takehomepay" placeholder="takehomepay" name="takehomepay" value="">
                                    <input type="hidden" class="form-control pph" placeholder="pph" name="pph_awal" value="">
                                    <input type="number" class="form-control pph" placeholder="pph" name="pph_ubah" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>



<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>PayrollDetail/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('.id').val(id).hide();
         $('.payrollid').val(data.payrollid);
         $('.userid').val(data.userid);
         $('.gaji_pokok').val(data.gaji_pokok);
         $('.tunjangan_jabatan').val(data.tunjangan_jabatan);
         $('.uang_makan').val(data.uang_makan);
         $('.uang_transport').val(data.uang_transport);
         $('.tunjangan_dinas').val(data.tunjangan_dinas);
         $('.lembur').val(data.lembur);
         $('.tambahan_lain').val(data.tambahan_lain);
         $('.jamsostek').val(data.jamsostek);
         $('.pph').val(data.pph);
         $('.bpjs').val(data.bpjs);
         $('.pinjaman').val(data.pinjaman);
         $('.potonganTerlambat').val(data.potonganTerlambat);
         $('.potongan_lain').val(data.potongan_lain);
         $('.total_penambah').val(data.total_penambah);
         $('.total_pengurang').val(data.total_pengurang);
         $('.takehomepay').val(data.takehomepay);
         $('.keterangan').val(data.keterangan);
         $('.total_hari_kerja').val(data.total_hari_kerja+" hari");
      }
    });
    var userid = $(ini).attr('data-userid');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>User/detail/"+userid,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('.noinduk').val(data.noinduk);
         $('.fullname').val(data.fullname);
         $('.npwp').val(data.npwp);
         $('.status_pajak').val(data.status_pajak);
         $('.periode').val("<?=$periode?>");
      }
    });
  }

  function clearForm() {    
     $('.id').val("");
     $('.payrollid').val("");
     $('.userid').val("");
     $('.gaji_pokok').val("");
     $('.tunjangan_jabatan').val("");
     $('.uang_makan').val("");
     $('.uang_transport').val("");
     $('.tunjangan_dinas').val("");
     $('.lembur').val("");
     $('.tambahan_lain').val("");
     $('.jamsostek').val("");
     $('.pph').val("");
     $('.bpjs').val("");
     $('.pinjaman').val("");
     $('.potonganTerlambat').val("");
     $('.potongan_lain').val("");
     $('.total_penambah').val("");
     $('.total_pengurang').val("");
     $('.takehomepay').val("");
     $('.keterangan').val("");

      $('.noinduk').val("");
      $('.fullname').val("");
      $('.npwp').val("");
      $('.status_pajak').val("");
      $('.periode').val("");
  }
</script>