<?php
$iduser = $this->session->userdata("id");
$user = $this->M_user->getDetail($iduser);
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Payroll
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Payroll </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
            <div class="pull-right">
                <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> Tambah Data</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Bulan</th>
              <th>Tahun</th>
              <th>Total Take Home Pay</th>
              <th>Catatan</th>
              <th>Status</th>
              <th>action</th>
            </tr>
            </thead>
              <tbody>
              <?php
              $no = 1;
              foreach ($rowData as $row) :
                  ?>
                  <tr>
                      <td><?=$no++;?></td>
                      <td><?=$row->bulan;?></td>
                      <td><?=$row->tahun;?></td>
                      <td><?="Rp ".number_format($row->total_thp, 0);?></td>
                      <td><?=$row->catatan;?></td>
                      <td><?php if($row->status == 0):?>
                              <label for="" class="label label-primary"> NEW </label>
                          <?php elseif($row->status == 1):?>
                              <label for="" class="label label-warning"> APPROVED ACCOUNTING </label>
                          <?php else:?>
                              <label for="" class="label label-success"> DONE </label>
                          <?php endif;?>
                      </td>
                      <td>
                          <a href="<?=site_url('Welcome/exportExcel/'.$row->id)?>" class="btn btn-xs btn-success"> <i class="fa fa-download"></i> </a>
                          <a href="<?=site_url('PayrollDetail/index/'.$row->id)?>" class="btn btn-xs btn-info"> <i class="fa fa-info"></i> </a>
                          <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalStatus" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-tag"></i></a>
                          <a href="<?=site_url('Payroll/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                  </tr>
              <?php endforeach;?>
              </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Payroll</h4>
      </div>
      <?=form_open("Payroll/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="bulan" class="col-sm-4 control-label">Bulan</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                      <select name="bulan" id="bulan" required class="form-control">
                          <option value="1">Januari</option>
                          <option value="2">Februari</option>
                          <option value="3">Maret</option>
                          <option value="4">April</option>
                          <option value="5">Mei</option>
                          <option value="6">Juni</option>
                          <option value="7">Juli</option>
                          <option value="8">Agustus</option>
                          <option value="9">September</option>
                          <option value="10">Oktober</option>
                          <option value="11">November</option>
                          <option value="12">Desember</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tahun" class="col-sm-4 control-label">Tahun</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="tahun" placeholder="tahun" name="tahun" value="<?=date('Y')?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="catatan" class="col-sm-4 control-label">Catatan</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="catatan" placeholder="catatan" name="catatan" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="modalStatus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Payroll</h4>
      </div>
      <?=form_open("Payroll/setStatus","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="status" class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                          <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                          <select name="status" id="status" required class="form-control">
                              <option value="0">Catatan</option>
                              <?php if($user->roleid == '4' || $user->roleid == '1'):?>
                              <option value="1">APRROVED ACCOUNTING</option>
                              <?php elseif($user->roleid == '2' || $user->roleid == '1'):?>
                                  <option value="2">DONE</option>
                              <?php endif?>
                          </select>
                      </div>
                  </div>
                <div class="form-group">
                  <label for="catatan" class="col-sm-4 control-label">Catatan</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="catatan" placeholder="catatan" name="catatan" value="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Payroll/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('.id').val(id).hide();
         $('#bulan').val(data.bulan);
         $('#tahun').val(data.tahun);
         $('#catatan').val(data.catatan);
        }
    });
  }

  function clearForm() {    
     $('.id').val("");
     $('#bulan').val("");
     $('#tahun').val("");
     $('#catatan').val("");
  }
</script>