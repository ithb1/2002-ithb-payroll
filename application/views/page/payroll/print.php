<?php
$periode = "";
switch ($pay->bulan){
	case 1: $periode = "Januari "; break;
	case 2: $periode = "Februari "; break;
	case 3: $periode = "Maret "; break;
	case 4: $periode = "April "; break;
	case 5: $periode = "Mei "; break;
	case 6: $periode = "Juni "; break;
	case 7: $periode = "Juli "; break;
	case 8: $periode = "Agustus "; break;
	case 9: $periode = "September "; break;
	case 10: $periode = "Oktober "; break;
	case 11: $periode = "November "; break;
	case 12: $periode = "Desember "; break;
}

$periode .= $pay->tahun;

// $this->apdf = new PDF();

// $this->apdf->setFilename($filename);

$this->apdf->fpdf('P','mm','A5');
$this->apdf->AliasNbPages();
$this->apdf->AddPage();


//HEADER
$this->apdf->setMargins(20,0);
$this->apdf->Ln();
$this->apdf->SetFont('Arial','B',12);
$this->apdf->Cell(0,6,$filename,0,2,'C');
$this->apdf->SetLineWidth(1);
$this->apdf->Cell(0,3,'','B',1,'C');
$this->apdf->Ln();

//Content
$this->apdf->SetFont('Helvetica','B',8);
$this->apdf->SetFillColor(153,203,103);
$this->apdf->SetHeader();
$this->apdf->Ln(2);
$this->apdf->SetLineWidth(0.5);

$this->apdf->Cell(20,6,"Periode ",0,0,'L');
$this->apdf->Cell(35,6,": ".$periode,0,0,'L');
$this->apdf->Cell(20,6,"NIK",0,0,'L');
$this->apdf->Cell(35,6,": ".$user->noinduk,0,1,'L');

$this->apdf->Cell(20,6,"Kehadiran",0,0,'L');
$this->apdf->Cell(35,6,": ".$payDetail->total_hari_kerja,0,0,'L');
$this->apdf->Cell(20,6,"Nama",0,0,'L');
$this->apdf->Cell(35,6,": ".$user->fullname,0,1,'L');

$this->apdf->Cell(20,6,"Status Pajak",0,0,'L');
$this->apdf->Cell(35,6,($user->status_pajak) ? ': AKTIF' : ': NON AKTIF',0,0,'L');
$this->apdf->Cell(20,6,"NPWP",0,0,'L');
$this->apdf->Cell(35,6,": ".$user->npwp,0,1,'L');
$this->apdf->Ln(5);

//=======================================
$this->apdf->SetLineWidth(0.3);
$this->apdf->SetFont('Helvetica','B',8);

$this->apdf->Cell(55,6,"PENAMBAH",1,0,'C');
$this->apdf->Cell(55,6,"PENGURANG",1,1,'C');

$this->apdf->SetFont('Helvetica','',8);

$this->apdf->Cell(15,20,"Tetap",1,0,'L');
$this->apdf->Cell(40,10,"Gaji Pokok : Rp ".number_format($payDetail->gaji_pokok,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"Jamsostek : Rp ".number_format($payDetail->jamsostek,0,',','.'),1,1,'L');
$this->apdf->Cell(15,0,"",0,0,'L');
$this->apdf->Cell(40,10,"Tj. Jabatan : Rp ".number_format($payDetail->tunjangan_jabatan,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"Pph : Rp ".number_format($payDetail->pph,0,',','.'),1,1,'L');

$this->apdf->Cell(15,30,"Variable",1,0,'L');
$this->apdf->Cell(40,10,"Uang Makan : Rp ".number_format($payDetail->uang_makan,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"BPJS Kesehatan : Rp ".number_format($payDetail->bpjs,0,',','.'),1,1,'L');
$this->apdf->Cell(15,0,"",0,0,'L');
$this->apdf->Cell(40,10,"Uang Transport : Rp ".number_format($payDetail->uang_transport,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"Pinjaman : Rp ".number_format($payDetail->pinjaman,0,',','.'),1,1,'L');
$this->apdf->Cell(15,0,"",0,0,'L');
$this->apdf->Cell(40,10,"Tj. Proyek/Dinas : Rp ".number_format($payDetail->tunjangan_dinas,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"",1,1,'L');
$this->apdf->Cell(55,10,"Lembur : Rp ".number_format($payDetail->lembur,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"",1,1,'L');

$this->apdf->Cell(55,10,"Tambahan Lain : Rp ".number_format($payDetail->tambahan_lain,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"Potongan Lain : Rp ".number_format($payDetail->potongan_lain,0,',','.'),1,1,'L');

$this->apdf->SetFont('Helvetica','B',8);
$this->apdf->Cell(55,10,"TOTAL : Rp ".number_format($payDetail->total_penambah,0,',','.'),1,0,'L');
$this->apdf->Cell(55,10,"TOTAL : Rp ".number_format($payDetail->total_pengurang,0,',','.'),1,1,'L');

$this->apdf->Cell(110,10,"Take Home Pay : Rp ".number_format($payDetail->takehomepay,0,',','.'),1,1,'L');
$this->apdf->SetFont('Helvetica','',8);
$this->apdf->Cell(110,10,"Keterangan : ".$payDetail->keterangan,1,1,'L');
?>
