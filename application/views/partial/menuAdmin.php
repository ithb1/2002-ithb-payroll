<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>

  <li class="<?=($this->uri->segment(1) == 'System')?'active':''?>"><a href="<?=site_url('System/Log')?>"><i class="fa fa-gear"></i> <span>System Log</span></a></li>
  <li class="treeview <?=($this->uri->segment(1) == 'Master')?'active':''?>">
    <a href="#">
      <i class="fa fa-gear"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="<?=($this->uri->segment(2) == 'Potongan')?'active':''?>"><a href="<?=site_url('Master/Potongan')?>"><span>Potongan</span></a></li>
        <li class="<?=($this->uri->segment(2) == 'Jabatan')?'active':''?>"><a href="<?=site_url('Master/Jabatan')?>"><span>Jabatan</span></a></li>
        <li class="<?=($this->uri->segment(2) == 'Karyawan')?'active':''?>"><a href="<?=site_url('Master/Karyawan')?>"><span>Karyawan</span></a></li>
    </ul>
  </li>
  <li class="<?=($this->uri->segment(1) == 'Absensi')?'active':''?>"><a href="<?=site_url('Absensi')?>"><i class="fa fa-calendar"></i> <span>Absensi</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Cuti')?'active':''?>"><a href="<?=site_url('Cuti')?>"><i class="fa fa-calendar-times-o"></i> <span>Cuti</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Proyek')?'active':''?>"><a href="<?=site_url('Proyek')?>"><i class="fa fa-calendar-check-o"></i> <span>Proyek</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Payroll')?'active':''?>"><a href="<?=site_url('Payroll')?>"><i class="fa fa-dollar"></i> <span>Payroll</span></a></li>
</ul>